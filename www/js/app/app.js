'use strict';

var gymseApp = angular.module('gymse', [
        // 'gymse.auth', 
        'ui.router', 
        'ngCordova'
    ])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            // .state('main', {
            //     url: '/main',
            //     // abstract: true,
            //     views: {
            //         '': {
            //             templateUrl: 'partials/main/main.html'
            //         },
            //         'header@main': {
            //             templateUrl: 'partials/main/header.html'
            //         },
            //         'content@main': {
            //             templateUrl: 'partials/main/content.html',
            //         },
            //     },
            //     controller: function(){
            //         // Инициализируем движок фреймворка
            //         var frameCore = new Framework7({
            //             swipePanel: 'left',
            //             swipePanelActiveArea: 50,
            //             swipeBackPage: false,
            //             // swipeout: false,
            //             router: false,
            //             // activeState: false
            //             // pushState: true
            //         });
                     
            //         // вместо jQuery используем встроенный Dom7, переменную можно назвать $, чтобы было привычнее, но если оставить $$, то в дальнейшем можно будет легко подключить jQuery, если понадобится
            //         var $$ = Dom7;
                     
            //         // инициализируем главную вьюху
            //         var mainView = frameCore.addView('.view-main', {
            //             dynamicNavbar: true, 
            //             domCache: true, // чтобы навигация работала без сбоев и с запоминанием scroll position в длинных списках
            //         });
            //     }
            // })
            .state('auth', {
                url: '/auth',
                templateUrl: 'partials/auth-page.html',
                controller: 'NotifyCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'partials/register-page.html'
            })
            .state('settings', {
                url: '/settings',
                templateUrl: 'partials/settings-page.html'
            })
            .state('messages', {
                url: '/messages',
                templateUrl: 'partials/messages-page.html'
            })
            .state('messages.id', {
                url: '/:id',
                templateUrl: 'partials/dialog-page.html',
                controller: function(){
                    var enterMessActive = $$('.enter-text-message').hasClass('active');

                    $$('.btn-open-send').click(function(){
                        if(!enterMessActive)
                            $$(this).parents('.enter-text-message').addClass('active')

                        return false;
                    })
                }
            })
            .state('news', {
                // parent: 'main',
                // abstract: true,
                url: '/news',
                templateUrl: 'partials/main-page.html',
                // views: {
                //     '': {
                //         templateUrl: 'partials/main-page.html'
                //     }
                // },
                controller: 'NewsCtrl'
            })
            .state('news.detail', {
                url: '/:id',
                templateUrl: 'partials/news-detail.html',
                controller: function(){
                    var contentSl = frameCore.swiper('.content-slider', {slidesPerView: 'auto',})
                }
            })
            .state('news.voting', {
                url: '/voting/:id',
                templateUrl: 'partials/voting-detail.html'
            })
            .state('news.voting1', {
                url: '/voting1/:id',
                templateUrl: 'partials/voting-detail1.html'
            })
            .state('news.voting2', {
                url: '/voting2/:id',
                templateUrl: 'partials/voting-detail2.html'
            })
            .state('schedule', {
                url: '/schedule',
                templateUrl: 'partials/schedule-page.html'
            })
            .state('feedback', {
                url: '/feedback',
                templateUrl: 'partials/feedback-page.html'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'partials/about-page.html',
                controller: function(){
                    var contentSl = frameCore.swiper('.content-slider', {slidesPerView: 'auto',})
                }
            });
        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/auth');
      }])
    // .directive('elastic', [
    //     '$timeout',
    //     function($timeout) {
    //         return {
    //             restrict: 'A',
    //             link: function($scope, element) {
    //                 $scope.initialHeight = $scope.initialHeight || element[0].style.height;
    //                 var resize = function() {
    //                     element[0].style.height = $scope.initialHeight;
    //                     element[0].style.height = "" + element[0].scrollHeight + "px";
    //                 };
    //                 element.on("blur keyup change", resize);
    //                 $timeout(resize, 0);
    //             }
    //         };
    //     }
    // ])
// .run([
//         '$rootScope', '$state', '$stateParams', 'SessionService', 
//         function ($rootScope, $state, $stateParams, SessionService) {

//             $rootScope.$state = $state;
//             $rootScope.$stateParams = $stateParams;

//             $rootScope.user = null;

//             // Здесь мы будем проверять авторизацию
//             $rootScope.$on('$stateChangeStart',
//               function (event, toState, toParams, fromState, fromParams) {
//                 SessionService.checkAccess(event, toState, toParams, fromState, fromParams);
//               }
//             );
//         }
//     ])

.controller('NotifyCtrl', function($scope, $cordovaLocalNotification) {

    document.addEventListener("deviceready", function () {

         // $scope.addNotification = function () {
           $cordovaLocalNotification.add({
             id: 1,
             title: 'Welcome to Gymse app',
             text: 'This is test notification',
             icon: 'http://www.optimizeordie.de/wp-content/plugins/social-media-widget/images/default/64/googleplus.png',
             // parameter documentation:
             // https://github.com/katzer/cordova-plugin-local-notifications#further-informations-1
           }).then(function () {
             console.log('callback for adding background notification');
           });
         // };

   }, false);

})
.controller('NewsCtrl', ['$scope', function($scope){

    $scope.news = [
        {
            "id": 1,
            "type": 'news',
            "date": '29 марта 2015',
            "message": 'Уважаемые клиенты. По вашим просьбам тренировку Strip Dance в 18:00 проведет Ольга Осипова.',
            "image": 'img/blank-news1.jpg',
        },
        {
            "id": 2,
            "type": 'news',
            "date": 'С 24 марта по 24 апреля 2015',
            "message": 'Уважаемые клиенты. Скидка 15% на все групповые тренинги, включая Fitball и Go-Go.',
            "image": 'img/blank-news2.jpg',
        },
        {
            "id": 3,
            "type": 'voting',
            "date": '25 марта 2015',
            "message": 'Оценка: что думаете о новой беговой дорожке',
        },
        {
            "id": 4,
            "type": 'voting',
            "date": '25 марта 2015',
            "message": 'Оцените работу нашего клуба',
            "ver": '1',
        },
        {
            "id": 5,
            "type": 'voting',
            "date": '25 марта 2015',
            "message": 'Оцените работу нашего клуба',
            "ver": '2',
        },
        {
    "id": "55969a49942580311b5f630c",
    "type": "news",
    "date": "29 марта 2015",
    "message": "ipsum sit minim laborum ullamco ut esse amet dolor exercitation laborum tempor incididunt ut labore anim voluptate cupidatat cillum excepteur",
    "image": "http://loremflickr.com/985/380?random=1"
  },
  {
    "id": "55969a49d32df341f22f3960",
    "type": "news",
    "date": "29 марта 2015",
    "message": "mollit consequat proident laborum aliqua aliqua officia esse cupidatat excepteur reprehenderit pariatur cillum aliqua aliquip ullamco elit commodo culpa ipsum",
    "image": "http://loremflickr.com/985/380?random=2"
  },
  {
    "id": "55969a4974d8f6954456e3a8",
    "type": "news",
    "date": "29 марта 2015",
    "message": "officia tempor cillum excepteur velit velit ex duis elit ullamco id officia proident est eu est eiusmod reprehenderit culpa qui",
    "image": "http://loremflickr.com/985/380?random=3"
  },
  {
    "id": "55969a49c8bf6e5ecf8c7979",
    "type": "news",
    "date": "29 марта 2015",
    "message": "culpa consequat dolore proident non qui labore aliquip nostrud Lorem adipisicing aliquip et tempor cillum nulla aliquip consequat consequat dolore",
    "image": "http://loremflickr.com/985/380?random=4"
  },
  {
    "id": "55969a499a6576396a3ef6ce",
    "type": "news",
    "date": "29 марта 2015",
    "message": "velit sit do nostrud occaecat ex culpa commodo nisi proident cupidatat elit cillum dolor elit exercitation commodo sint proident consectetur",
    "image": "http://loremflickr.com/985/380?random=5"
  },
  {
    "id": "55969a49bc913c32662b9c09",
    "type": "news",
    "date": "29 марта 2015",
    "message": "amet pariatur velit eu nulla non ex adipisicing dolor excepteur aliqua aute aliqua in cupidatat esse tempor elit exercitation Lorem",
    "image": "http://loremflickr.com/985/380?random=6"
  },
  {
    "id": "55969a492cc9bf1b53b51a8f",
    "type": "news",
    "date": "29 марта 2015",
    "message": "non adipisicing anim ipsum et in dolor officia voluptate fugiat voluptate culpa mollit nulla labore reprehenderit ipsum aute cupidatat ullamco",
    "image": "http://loremflickr.com/985/380?random=7"
  },
  {
    "id": "55969a49e40ea754e2219a0a",
    "type": "news",
    "date": "29 марта 2015",
    "message": "Lorem eiusmod veniam laborum adipisicing ipsum fugiat in proident id ullamco adipisicing id dolor esse sunt exercitation ea proident velit",
    "image": "http://loremflickr.com/985/380?random=8"
  },
  {
    "id": "55969a49df6201c5529aa1d9",
    "type": "news",
    "date": "29 марта 2015",
    "message": "voluptate nostrud et qui eiusmod do laboris est nisi labore do ipsum laborum mollit veniam velit est enim laboris quis",
    "image": "http://loremflickr.com/985/380?random=9"
  },
  {
    "id": "55969a49934d7f1d9680a7b5",
    "type": "news",
    "date": "29 марта 2015",
    "message": "veniam incididunt ea Lorem cupidatat velit nostrud duis aliquip sint quis occaecat excepteur cupidatat aute excepteur consectetur magna nisi sit",
    "image": "http://loremflickr.com/985/380?random=10"
  },

    ]
}])
.directive('gmsItems', function(){
    return{
        restrict: 'E',
        replace: true,
        // scope: true,
        template: '<ng-include src="getTemplateUrl()" include-replace/>',
        // templateUrl: 'partials/items/item-news.html',
        // templateUrl: function(elem, attrs) {
           // return attrs.templateUrl || 'partials/items/item-news.html'
       // },
        controller: function($scope) {
          $scope.getTemplateUrl = function() {
              return 'partials/items/item-'+$scope.item.type+'.html';
          }
        }
    };
})
.directive('includeReplace', function () {
    return {
        require: 'ngInclude',
        restrict: 'A', /* optional */
        link: function (scope, el, attrs) {
            el.replaceWith(el.children());
        }
    };
})