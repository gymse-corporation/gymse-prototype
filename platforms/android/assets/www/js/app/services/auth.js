'use strict';

angular.module('gymse.auth')
	.service('SessionService', [
		'$injector',
		function($injector) {
			'use strict';

			this.checkAccess = function(event, toState, toParams, fromState, fromParams) {
				var $scope = $injector.get('$rootScope'),
					$sessionStorage = $injector.get('$sessionStorage');

				if(toState.data !== undefined) {
					if (toState.data.noAuth !== undefined && toState.data.noAuth){
						// действия перед входом без авторизации
					}
				} else {
					// Вход с авторизацией
					if ($sessionStorage.user) {
						$scope.$root.user = $sessionStorage.user;
					} else {
						// Если не авторизован - переадресация на страницу авторизации
						event.preventDefault();
						$scope.$state.go('auth');
					}
				}
			};
		}
	])