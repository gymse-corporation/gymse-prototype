var gulp = require('gulp'), // Gulp JS
    sass = require('gulp-sass'), // Плагин для Sass
//    bump = require('gulp-bump'), // Обновление версий пакетов проекта
    csscomb = require('gulp-csscomb'), // Форматирование css
    minifyCss = require('gulp-minify-css'), // Минификация CSS
    autoprefixer = require('gulp-autoprefixer'), // Подстановка префиксов
//    prettify = require('gulp-prettify'), // Форматирование html
    uglify = require('gulp-uglify'), // Минификация js
    plumber = require('gulp-plumber'),
    gulpif = require('gulp-if'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
//    imagemin = require('gulp-imagemin'), // Минификация изображений
    spritesmith = require('gulp.spritesmith'), // Генератор спрайтов и CSS переменных
    browserSync = require('browser-sync'), // Локальный сервер и автоперезагрузка страницы
    reload = browserSync.reload,
    path = require('./paths'),
    pkg = require('./package.json');

// Static Server + watching scss/html files

gulp.task('server',['sass','sprite','compress'], function() {
    browserSync({
      server: {
          baseDir: path.dist
      }
    });
    gulp.watch('www/styles/scss/**/*.scss', ['sass']);
    gulp.watch('www/img/sprite/*.*', ['sprite']);
    gulp.watch('www/js/**/*.js', ['compress']).on('change', reload);
    gulp.watch('www/**/*.html').on('change', reload);
});

// Компиляция sass фалов
gulp.task('sass', function () {
  // Путь к sass файлам
  gulp.src(['**/*.scss'], {
    cwd: path.scssPath
  })
    .pipe(plumber())
  // Вывод ошибки компиляции
    .pipe(sass({
        errLogToConsole: true
    }))
    // Генерация префиксов
    .pipe(autoprefixer({
        browsers: [
            'Android >= ' + pkg.browsers.android,
			'Chrome >= ' + pkg.browsers.chrome,
//			'Firefox >= ' + pkg.browsers.firefox,
//			'Explorer >= ' + pkg.browsers.ie,
			'iOS >= ' + pkg.browsers.ios,
//			'Opera >= ' + pkg.browsers.opera,
//			'Safari >= ' + pkg.browsers.safari
        ],
        cascade: false
    }))
    
  // Минификация стилей
    .pipe(gulpif(!gutil.env.debug, minifyCss()))
    .pipe(gulpif(gutil.env.csscomb, csscomb()))
  
  // Путь скомпилированного css файла
    .pipe(gulp.dest(path.styles))
    .pipe(reload({stream: true}));
})

// Генерация спрайтов
gulp.task('sprite', function() {
    var spriteData = 
        gulp.src(['sprite/*.*'], {
            cwd: path.images // Путь, откуда берем картинки для спрайта
        })
            .pipe(plumber())
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.scss',
                cssFormat: 'scss',
                algorithm: 'binary-tree',
                padding: 5,
//                cssTemplate: 'sass.template.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = 'gms-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest(path.images)); // Путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(path.spritePath)); // Путь, куда сохраняем стили
});

// Минификация js
gulp.task('compress', function() {
  return gulp.src(['**/*.js', '!**/*.min.js', '!min/**', '!lib/**'], {
      cwd: path.scripts
  })
    .pipe(plumber())
    .pipe(concat('common.min.js'))
    .pipe(gulpif(!gutil.env.debug, uglify()))
    .pipe(gulp.dest(path.minScripts))
});


gulp.task('default', ['server']);